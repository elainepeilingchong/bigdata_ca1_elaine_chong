function insertMovie(title, writer, year, actors)
{
    db.movies.insert({
    title: title,
    writer: writer,
    year: year,
    actors: actors
    })
}

function insertPost(username, title, body)
{
    db.posts.insert({
    username: username,
    title: title,
    body: body
    })
}

function insertComment(username, comment, titleToRef)
{
    db.comments.insert({
    username: username,
    comment: comment,
    post  : {$ref : "posts", $id : db.posts.findOne({title : titleToRef})._id}
    })
}
