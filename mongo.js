//MONGO
load("/Users/elainechong/Desktop/Sem_5/CA1/MONGO/function.js");
db.movies.insert({
    title: "Fight Club",
    writer: "Chuck Palahniuk",
    year: 1999,
    actors: ["Brad Pitt", "Edward Norton"]
})
//load function
insertMovie("Pulp Fiction", "Quentin Tarantino", 1994, ["John Travolta", "Uma Thurman" ])

insertMovie("Inglorious Basterds", "Quentin Tarantino", 2009, ["Brad Pitt", "Diane Kruger ","Eli Roth" ])
db.movies.insert({
    title: "The Hobbit: An Unexpected Journey",
    writer: "J.R.R. Tolkein",
    year: 2012,
    franchise: "The Hobbit"
})
db.movies.insert({
    title: "The Hobbit: The Desolation of Smaug",
    writer: "J.R.R. Tolkein",
    year: 2013,
    franchise: "The Hobbit"
})

db.movies.insert({
    title: "The Hobbit: The Battle of the Five Armies",
    writer: "J.R.R. Tolkein",
    year: 2012,
    franchise: "The Hobbit",
    synopsis: "Bilbo and Company are forced to engage in a war against an array of combatants and keep the Lonely Mountain from falling into the hands of a rising darkness."
})
db.movies.insert({
    title: "Pee Wee Herman's Big Adventure"
})
db.movies.insert({
    title: "Avatar"
})

//3
//3.1
db.movies.count() //8
db.movies.find().pretty()
//3.2
db.movies.find({writer :"Quentin Tarantino"}).pretty()
//3.3
db.movies.find({actors : "Brad Pitt"}).pretty();
//3.4
db.movies.find({franchise : "The Hobbit"}).pretty()
//3.5
var nineties = {}; 
nineties['$lte'] = 1999; 
nineties['$gte'] = 1990; 
db.movies.find({year : nineties}).pretty()
//3.6
db.movies.find({ $or :[
    {year:{$lt: 2000}}, 
    {year:{$gt: 2010}}
]}).pretty()

//4
//4.1
db.movies.find({title:"The Hobbit: An Unexpected Journey"}).pretty()
db.movies.update(
{title : "The Hobbit: An Unexpected Journey"}, {$set : {synopsis : "A reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home - and the gold within it - from the dragon Smaug."}})
//4.2
db.movies.find({title:"The Hobbit: The Desolation of Smaug"}).pretty()
db.movies.update(
{title : "The Hobbit: The Desolation of Smaug"}, {$set : {synopsis : "The dwarves, along with Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug. Bilbo Baggins is in possession of a mysterious and magical ring."}})
//4.3
db.movies.find({title:"Pulp Fiction"}).pretty()
db.movies.update(
{title : "Pulp Fiction"}, { $push: { actors: "Samuel L. Jackson" }})

//5
//5.1
db.movies.find({synopsis:/Bilbo/}).pretty()
//5.2
db.movies.find({synopsis:/Gandalf/}).pretty()
//5.3
db.movies.find({ $and: [{synopsis:/Bilbo/},{synopsis:{$not :/Gandalf/}}]}).pretty()
//5.4
db.movies.find({ $or: [{synopsis:/dwarves/},{synopsis:/hobbit/}]}).pretty()
//5.5
db.movies.find({ $and: [{synopsis:/gold/},{synopsis:/dragon/}]}).pretty()

//6
db.movies.remove( { title : "Pee Wee Herman's Big Adventure" } )
db.movies.remove( { title: "Avatar" } )

//7
db.users.insert({
    username: "GoodGuyGreg",
    first_name: "Good Guy",
    last_name: "Greg",
})
db.users.insert({
    username: "ScumbagSteve",
    full_name:{first : "Good Guy",last : "Steve" } 
   
})

insertPost("GoodGuyGreg", "Passes out at party", "Wakes up early and cleans house")
insertPost("GoodGuyGreg", "Steals your identity", "Raises your credit score")
insertPost("GoodGuyGreg", "Reports a bug in your code", "Sends you a Pull Request")
insertPost("ScumbagSteve", "Borrows something", "Sells it")
insertPost("ScumbagSteve", "Borrows everything", "The end")
insertPost("ScumbagSteve", "Forks your repo on github", "Sets to private")

insertComment("GoodGuyGreg","Hope you got a good deal!","Borrows something")
insertComment("GoodGuyGreg","What's mine is yours!","Borrows everything")
insertComment("GoodGuyGreg","Don't violate the licensing agreement!","Forks your repo on github")
insertComment("ScumbagSteve","It still isn't clean","Passes out at party")
insertComment("ScumbagSteve","Denied your PR cause I found a hack","Reports a bug in your code")

//8
//8.1
db.users.find().pretty()
//8.2
db.posts.find().pretty()
//8.3
db.posts.find({username:"GoodGuyGreg"}).pretty()
//8.4
db.posts.find({username:"ScumbagSteve"}).pretty()
//8.5
db.comments.find().pretty()
//8.6
db.comments.find({username:"GoodGuyGreg"}).pretty()
//8.7
db.comments.find({username:"ScumbagSteve"}).pretty()
//8.8
db.comments.find({'post.$id' : db.posts.findOne({title : "Reports a bug in your code"})._id }).pretty()

