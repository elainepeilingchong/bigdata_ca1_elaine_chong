INSERT INTO bigdata_ca1_elaine_chong.public.posts(username, title, body) VALUES ('GoodGuyGreg', 'Passes out at party Guy', 'Wakes up early and cleans house');
INSERT INTO bigdata_ca1_elaine_chong.public.posts(username, title, body) VALUES ('GoodGuyGreg', 'Steals your identity', 'Raises your credit score');
INSERT INTO bigdata_ca1_elaine_chong.public.posts(username, title, body) VALUES ('GoodGuyGreg', 'Reports a bug in your code', 'Sends you a Pull Request');
INSERT INTO bigdata_ca1_elaine_chong.public.posts(username, title, body) VALUES ('ScumbagSteve', 'Borrows something', 'Sells it');
INSERT INTO bigdata_ca1_elaine_chong.public.posts(username, title, body) VALUES ('ScumbagSteve', 'Borrows everything', 'The end');
INSERT INTO bigdata_ca1_elaine_chong.public.posts(username, title, body) VALUES ('ScumbagSteve', 'Forks your repo on github', 'Sets to private');
