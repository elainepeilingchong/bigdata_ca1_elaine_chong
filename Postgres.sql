#POSTGRESQL
CREATE DATABASE bigdata_ca1_elaine_chong

CREATE TABLE movies( 
    movie_id SERIAL PRIMARY KEY,
    title text, 
    writer text,
    year integer,
    franchise text,
    synopsis text
);
CREATE TABLE actors( 
    actor_id SERIAL PRIMARY KEY,
    name text
);
CREATE TABLE movies_actors( 
    movie_id integer REFERENCES movies NOT NULL, 
    actor_id integer REFERENCES actors NOT NULL,
UNIQUE(movie_id, actor_id)
);


INSERT INTO movies (title, writer, year, franchise,  synopsis) VALUES('Fight Club','Chuck Palahniuk',1999,'',''),('Pulp Fiction','Quentin Tarantino',1994,'',''),('Inglorious Basterds','Quentin Tarantino',2009,'',''),('The Hobbit: An Unexpected Journey','J.R.R. Tolkein',2012,'The Hobbit',''),('The Hobbit: The Desolation of Smaug','J.R.R. Tolkein',2013,'The Hobbit',''),('The Hobbit: The Battle of the Five Armies','J.R.R. Tolkein',2012,'The Hobbit','Bilbo and Company are forced to engage in a war against an array of combatants and keep the Lonely Mountain from falling into the hands of a rising darkness.'),('Pee Wee Herman''s Big Adventure','',NULL,'',''),('Avatar','',NULL,'','');

INSERT INTO actors (name) VALUES ('Brad Pitt'),('Edward Norton'),('John Travolta'),('Uma Thurman'),('Diane Kruger'),('Eli Roth');

CREATE OR REPLACE FUNCTION add_movies_actors(movie_name text, actor_name text) 
RETURNS boolean as $$
DECLARE 
    did_insert boolean :=false;
    the_actor_id integer;
BEGIN
    SELECT actor_id INTO the_actor_id FROM actors WHERE name = actor_name;
    IF the_actor_id is NULL THEN
        INSERT INTO actors(name) VALUES (actor_name) RETURNING actor_id INTO the_actor_id; 
        did_insert:= true;
    END IF;
    INSERT INTO movies_actors (movie_id,actor_id) VALUES ((SELECT movie_id FROM movies WHERE title=movie_name), the_actor_id); 
    RETURN did_insert;
END;
$$ LANGUAGE plpgsql;

select add_movies_actors('Fight Club','Brad Pitt');
select add_movies_actors('Fight Club','Edward Norton');
select add_movies_actors('Pulp Fiction','John Travolta');
select add_movies_actors('Pulp Fiction','Uma Thurman');
select add_movies_actors('Inglorious Basterds','Brad Pitt');
select add_movies_actors('Inglorious Basterds','Diane Kruger');
select add_movies_actors('Inglorious Basterds','Eli Roth');

#3
#3.1
SELECT * FROM movies;
#3.2
SELECT * FROM movies WHERE writer = 'Quentin Tarantino';
#3.3
SELECT * FROM movies m, actors a,movies_actors ma WHERE  m.movie_id = ma.movie_id AND ma.actor_id = a.actor_id AND a.name = 'Brad Pitt';
#3.4
SELECT * FROM movies WHERE franchise = 'The Hobbit';
#3.5
SELECT * FROM movies WHERE year >= 1990 AND year <=1999;
#3.6
SELECT * FROM movies WHERE year < 2000 OR year >2010;

#4
#4.1
UPDATE movies SET synopsis= 'A reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home - and the gold within it - from the dragon Smaug.' WHERE title = 'The Hobbit: An Unexpected Journey';
#4.2
UPDATE movies SET synopsis= 'The Hobbit: The Desolation of Smaug"}, {$set : {synopsis : "The dwarves, along with Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug. Bilbo Baggins is in possession of a mysterious and magical ring.' WHERE title = 'The Hobbit: The Desolation of Smaug';

#4.3
select add_movies_actors('Pulp Fiction','Samuel L. Jackson');

#5
#5.1
SELECT * FROM movies WHERE synopsis LIKE '%Bilbo%';
#5.2
SELECT * FROM movies WHERE synopsis LIKE '%Gandalf%';
#5.3
SELECT * FROM movies WHERE synopsis LIKE '%Bilbo%' AND synopsis NOT LIKE '%Gandalf%';
#5.4
SELECT * FROM movies WHERE synopsis LIKE '%dwarves%' OR synopsis LIKE '%hobbit%';
#5.5
SELECT * FROM movies WHERE synopsis LIKE '%gold%' AND synopsis LIKE '%dragon%';

#6
#6.1
DELETE FROM movies WHERE title= 'Pee Wee Herman''s Big Adventure';
#6.2
DELETE FROM movies WHERE title= 'Avatar';

#7
CREATE TABLE users(   
    user_id SERIAL PRIMARY KEY,
    username text,
    first_name text,
    second_name text
);

CREATE TABLE posts(   
    post_id SERIAL PRIMARY KEY,
    username text,
    title text,
    body text
);
CREATE TABLE comments(   
    comment_id SERIAL PRIMARY KEY,
    post_id integer REFERENCES posts NOT NULL, 
    username text,
    comment text
);


INSERT INTO users (username, first_name, second_name) VALUES ('GoodGuyGreg','Good Guy','Greg'),('ScumbagSteve','Scumbag','Steve');

INSERT INTO posts (username,title ,body) VALUES ('GoodGuyGreg','Passes out at party Guy','Wakes up early and cleans house'),('GoodGuyGreg','Steals your identity','Raises your credit score'),('GoodGuyGreg','Reports a bug in your code','Sends you a Pull Request'),('ScumbagSteve','Borrows something','Sells it'),('ScumbagSteve','Borrows everything','The end'),('ScumbagSteve','Forks your repo on github','Sets to private');

INSERT INTO comments (username,comment,post_id) VALUES ('GoodGuyGreg','Hope you got a good deal!',4),('GoodGuyGreg','What''s mine is yours!',5),('GoodGuyGreg','Don''t violate the licensing agreement!',6),('ScumbagSteve','It still isn''t clean',1),('ScumbagSteve','Denied your PR cause I found a hack',3);

#8
#8.1
SELECT * FROM users;
#8.2
SELECT * FROM posts;
#8.3
SELECT * FROM posts WHERE username = 'GoodGuyGreg';
#8.4
SELECT * FROM posts WHERE username = 'ScumbagSteve';
#8.5
SELECT * FROM comments;
#8.6
SELECT * FROM comments WHERE username = 'GoodGuyGreg';
#8.7
SELECT * FROM comments WHERE username = 'ScumbagSteve';
#8.8
SELECT * FROM comments c,posts p WHERE c.post_id = p.post_id AND p.title = 'Reports a bug in your code'; 

